Installation
============

- Copy the library to a persistent path on your system
- Open KiCad -> Settings -> Manage Footprint Libraries...
  - select the global tab
  - use the folder icon to add all the *.pretty folders (hold shift to select them all)
- do the same for the symbols

Always check if the symbols and footprints are correct as I haven't used all of them.
